﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeCrud.Models
{
    public class Employee
    {
       
            [Key]
            public int Id { get; set; }
            [Required]
            [StringLength(30)]
            public string Name { get; set; }
            [Required]
            [StringLength(20)]
            public string State { get; set; }
            [Required]
            [StringLength(30)]
            public string City { get; set; }
            [Required]
            public decimal Salary { get; set; }

            public int ManagerId { get; set; }
        }
}
