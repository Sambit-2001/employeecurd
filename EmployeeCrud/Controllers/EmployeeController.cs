﻿using EmployeeCrud.Data;
using EmployeeCrud.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeCrud.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApplicationContext context;


        public EmployeeController(ApplicationContext context)
        {
            this.context = context;
        }


        public IActionResult Index()
        {
            var result = context.Employees.ToList();

            var joinTable = (from r in context.Employees
                             join mng in context.Managers on r.ManagerId equals mng.Id 
                             select new EmployeeData()
                             {Id=r.Id,
                               Name=r.Name,
                               Salary=r.Salary,
                               City=r.City,
                               State=r.State,
                               ManagaerId=mng.Id,
                               ManagerName=mng.Name
                             }
                             ).ToList();


            ViewBag.data = joinTable;


            return View(result);
            }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.manager = context.Managers.ToList();
            return View();
        }
        [HttpPost]
        public IActionResult Create(string Name, string State, string City, decimal Salary, string mng)

        {

            var emp = new Employee()
            {
                Name = Name,
                State=State,
                City=City,
                Salary=Salary,
               ManagerId=Convert.ToInt32(mng)


            };
            context.Employees.Add(emp);
            context.SaveChanges();
            TempData["Create"] = "Employee Created!!";
            return RedirectToAction("Index");


            /* if (ModelState.IsValid)
             {

                 var emp = new Employee()
                 {
                     Name = model.Name,
                     State = model.State,
                     City = model.City,
                     Salary = model.Salary,
                     ManagerId = model.ManagerId

                 };
                 context.Employees.Add(emp);
                 context.SaveChanges();
                 TempData["Create"] = "Employee Created!!";
                 return RedirectToAction("Index");
             }
             else
             {
                 TempData["C_error"] = "Empty field can't Submit!!";
                 return View();
             }
 */

        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {
            var emp = context.Employees.SingleOrDefault(e => e.Id == Id);

            /*  var emp =  from e in context.Employees
                                 where e.Id == id 
                                 select new Employee() ;

              Employee data = emp.FirstOrDefault();*/
            ViewBag.manager = context.Managers.ToList();
            var result = new Employee()
            {
                Name = emp.Name,
                State = emp.State,
                City = emp.City,
                Salary = emp.Salary,
                ManagerId=emp.ManagerId
            };

            return View(result);
        }
        [HttpPost]
        public IActionResult Edit(int Id,string Name, string State, string City, decimal Salary, string mng)
        {
            var emp = new Employee()
            {
                Id = Id,
                Name =Name,
                State =State,
                City =City,
                Salary =Salary,
                ManagerId = Convert.ToInt32(mng)
            };
            context.Employees.Update(emp);
            context.SaveChanges();
            TempData["Edit"] = "Employee Updated!!";
            return RedirectToAction("Index");
        }
        public IActionResult Delete(int Id)
        {
            var emp = context.Employees.SingleOrDefault(e => e.Id == Id);
            context.Employees.Remove(emp);
            context.SaveChanges();
            TempData["Delete"] = "Employee Deleted!!";
            return RedirectToAction("Index");
        }
    }
}
public class EmployeeData
{

    public int Id { get; set; }
    
    public string Name { get; set; }
   
    public string State { get; set; }
   
    public string City { get; set; }

    public decimal Salary { get; set; }
    public int ManagaerId { get; set; }
    public string ManagerName { get; set; }
}