﻿using EmployeeCrud.Data;
using EmployeeCrud.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeCrud.Controllers
{
    public class ManagerController : Controller
    {
        private readonly ApplicationContext context;
        public ManagerController(ApplicationContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            ViewBag.manager = context.Managers.ToList();
            return View();
        }
        [HttpGet]
        public IActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public IActionResult Create(Manager model)
        {
            if (ModelState.IsValid)
            {
                var mng = new Manager()
                {
                    Name = model.Name,
                    State = model.State,
                    City = model.City,
                    Salary = model.Salary
                };
                context.Managers.Add(mng);
                context.SaveChanges();
                TempData["Create"] = "Manager Created!!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["C_error"] = "Empty field can't Submit!!";
                return View(model);
            }

        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var emp = context.Managers.SingleOrDefault(e => e.Id == id);

            /*  var emp =  from e in context.Employees
                                 where e.Id == id 
                                 select new Employee() ;

              Employee data = emp.FirstOrDefault();*/

            var result = new Manager()
            {
                Name = emp.Name,
                State = emp.State,
                City = emp.City,
                Salary = emp.Salary
            };

            return View(result);
        }
        [HttpPost]
        public IActionResult Edit(Manager model)
        {
            var emp = new Manager()
            {
                Id = model.Id,
                Name = model.Name,
                State = model.State,
                City = model.City,
                Salary = model.Salary
            };
            context.Managers.Update(emp);
            context.SaveChanges();
            TempData["Edit"] = "Manager Updated!!";
            return RedirectToAction("Index");
        }
        public IActionResult Delete(int id)
        {
            var emp = context.Managers.SingleOrDefault(e => e.Id == id);
            context.Managers.Remove(emp);
            context.SaveChanges();
            TempData["Delete"] = "Manager Deleted!!";
            return RedirectToAction("Index");
        }
    }
}
